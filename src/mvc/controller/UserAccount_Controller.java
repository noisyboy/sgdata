package mvc.controller;

import dao.UserAccount_DAO;
import mvc.model.User_Model;
import mvc.view.DashboardAdmin_view;
import mvc.view.DashboardCapturista_view;
import mvc.view.DashboardTechnician_view;
import mvc.view.LogIn_view;
import org.apache.commons.codec.digest.DigestUtils;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserAccount_Controller implements ActionListener, DocumentListener {

    public UserAccount_Controller() {
        uaDAO = new UserAccount_DAO();
        lv = new LogIn_view();
        lv.setVisible(true);
        lv.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        lv.btnSiguienteU.addActionListener(this::actionPerformed); // boton que consulta la cuenta
        lv.btnSiguienteP.addActionListener(this::actionPerformed); // boton que consulta la contraseña
        lv.txtUser.getDocument().addDocumentListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        um = new User_Model();

        if(e.getSource() == lv.btnSiguienteU){
            BotonSiguienteUser();
        }

        if(e.getSource() == lv.btnSiguienteP){
            BotonSiguientePass();
        }
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        lv.txtUser.setBackground(Color.WHITE);
        lv.txtUser.setForeground(Color.BLACK);
        lv.lbAlert.setText("");
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        lv.txtUser.setBackground(Color.WHITE);
        lv.txtUser.setForeground(Color.BLACK);
        lv.lbAlert.setText("");
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }

    public void BotonSiguientePass() {
        um.setPass(lv.txtPass.getText()); // establece el texto de JPassworField al atributo pass
        String decoding = DigestUtils.sha1Hex(um.getPass()); // decodifica el pass para compararlo
        
        if(uaDAO.getRsData().get(5).equals(decoding) && uaDAO.getRsData().get(6).equals("addmin")){
            lv.dispose();
            new DashboardAdmin_view().setVisible(true);
        } else if(uaDAO.getRsData().get(5).equals(decoding) && uaDAO.getRsData().get(6).equals("capp")){
            lv.dispose();
            new DashboardCapturista_view().setVisible(true);
        }else if(uaDAO.getRsData().get(5).equals(decoding) && uaDAO.getRsData().get(6).equals("techh")){
            lv.dispose();
            new DashboardTechnician_view().setVisible(true);
        }
    }

    public void BotonSiguienteUser() {
        um.setEmail(lv.txtUser.getText());
        if(um.getEmail().isEmpty()){
            lv.txtUser.setBackground(Color.RED);
            lv.lbAlert.setText("-- Campo Obligatorio --");
            Toolkit.getDefaultToolkit().beep();
//            JOptionPane.showMessageDialog(null, "Campo obligatorio",
//                    "Obligatorio",JOptionPane.WARNING_MESSAGE);
        }else {
            try {
                if(uaDAO.CheckAccount(uaDAO.getSqlCheckAccout(),um.getEmail())){
                    if(uaDAO.getRsData().get(0).equals("not")){
                        lv.txtUser.setBackground(Color.RED);
                        lv.txtUser.setForeground(Color.WHITE);
                        lv.lbAlert.setText("-- Su cuenta no existe --");
                        Toolkit.getDefaultToolkit().beep();
                    }else if(uaDAO.getRsData().get(7).equals("inactive")){
                        lv.txtUser.setBackground(Color.RED);
                        lv.txtUser.setForeground(Color.WHITE);
                        lv.lbAlert.setText("-- Su cuenta está inactiva --");
                        Toolkit.getDefaultToolkit().beep();
                    } else {
                        String name = uaDAO.getRsData().get(1)+ " "+uaDAO.getRsData().get(2)+" "+
                                uaDAO.getRsData().get(3);
                        lv.lbUserDB.setText(name);
                        lv.cajaTituloAcceder.setVisible(false);
                        lv.cajaTextosUser.setVisible(false);
                        lv.cajaBotonesUser.setVisible(false);

                        lv.cajaTituloBienvenida.setVisible(true);
                        lv.cajaTextosPass.setVisible(true);
                        lv.cajaBotonesPass.setVisible(true);
                    }
                }
            }catch (Exception ec) {
                ec.printStackTrace();
            }finally {
            }
        }
    }

    User_Model um;
    UserAccount_DAO uaDAO;
    LogIn_view lv;
}