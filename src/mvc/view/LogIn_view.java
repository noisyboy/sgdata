package mvc.view;

import javax.swing.*;
import java.awt.*;

public class LogIn_view extends JFrame {
    public LogIn_view() {
        setTitle("LogIn");
        setSize(350,550);
        setUndecorated(true); // elimina el marco del JFrame
        setLocationRelativeTo(null); // Establece posicion centrada
        setResizable(false);

        add(PanelLogIn(), BorderLayout.CENTER);
    }

    public JPanel PanelLogIn() {
        panelFlow = new JPanel(new FlowLayout());

        /*
        * Se crean instancias de las Cajas a usar
        */
        cajaPrincipal = new Box(BoxLayout.Y_AXIS); // caja vertical --Principal
        cajaLogo = new Box(BoxLayout.X_AXIS); // caja horizontal --Logo
        cajaTituloAcceder = new Box(BoxLayout.X_AXIS); // caja horizontal para titulo Acceder
        cajaDatos = new Box(BoxLayout.Y_AXIS); // caja vertical --Datos
        cajaTextosUser = new Box(BoxLayout.Y_AXIS); // caja vertical visible = true --JLabel y JTextField
        cajaBotonesUser = new Box(BoxLayout.X_AXIS); // caja horizontal visible = true --JButons
        cajaAlert = new Box(BoxLayout.X_AXIS); // caja horizontal para las alertas
        cajaProgressBar = new Box(BoxLayout.X_AXIS); // caja horizontal para las alertas

        cajaTituloBienvenida = new Box(BoxLayout.X_AXIS); // caja horizontal para titulo bienvenida
        cajaTituloBienvenida.setVisible(false);
        cajaUserDB = new Box(BoxLayout.X_AXIS); // caja horizaontal para el user de la DataBase
        cajaTextosPass = new Box(BoxLayout.Y_AXIS); // caja vertical visible = false
        cajaTextosPass.setVisible(false);
        cajaBotonesPass = new Box(BoxLayout.X_AXIS); // caja horizontal visible = false
        cajaBotonesPass.setVisible(false);

        /*
        * Se crean instancias de los tipos de letra
        */
        Font fontEncabezado = new Font("Ebrima",Font.BOLD,20);
        Font fontLabel = new Font("Ebrima",Font.PLAIN,16);
        Font fontTxt = new Font("Bookman Old Style",Font.PLAIN,14);
        Font fontBtn = new Font("Georgia",Font.PLAIN,14);
        Font fontAlert = new Font("Bookman Old Style",Font.BOLD,14);

        /*
        * Comienzan las instancias de los componentes
        */
        lbLogo = new JLabel(new ImageIcon("./img/coca-cola.png"));
        cajaLogo.add(lbLogo);

        lbAcceder = new JLabel("Acceder");
        lbAcceder.setFont(fontEncabezado);
        cajaTituloAcceder.add(lbAcceder);

        lbUserDB = new JLabel(""); // Etiqueta para el usuario que se consulta de la DB
        lbUserDB.setFont(fontLabel);
        cajaUserDB.add(lbUserDB);

        lbUser = new JLabel("Ingrese su Usuario");
        lbUser.setFont(fontLabel);
        cajaTextosUser.add(lbUser);

        cajaTextosUser.add(Box.createVerticalStrut(5));

        txtUser = new JTextField("");
        txtUser.setFont(fontTxt);
        txtUser.setHorizontalAlignment(JTextField.CENTER);
        txtUser.setPreferredSize(new Dimension(10,25));
        cajaTextosUser.add(txtUser);

        btnCrear = new JButton("Crear Cuenta");
        btnCrear.setFont(fontBtn);
        cajaBotonesUser.add(btnCrear);

        cajaBotonesUser.add(Box.createHorizontalStrut(10));

        btnSiguienteU = new JButton("Siguiente");
        btnSiguienteU.setFont(fontBtn);
        cajaBotonesUser.add(btnSiguienteU);

        lbAlert = new JLabel("");
        lbAlert.setFont(fontAlert);
        lbAlert.setForeground(Color.RED);
        cajaAlert.add(lbAlert);

        progressBar = new JProgressBar();
        progressBar.setBackground(Color.GRAY);
        progressBar.setVisible(false);
        cajaProgressBar.add(progressBar);


        /*
         * Controles inicializados en falso
         * Se activarán cundo se valide la cuenta
         */
        lbBievenida = new JLabel("Te damos la bienvenida");
        lbBievenida.setFont(fontEncabezado);
        cajaTituloBienvenida.add(lbBievenida);

        lbPass = new JLabel("Ingrese su contraseña");
        lbPass.setFont(fontLabel);
        cajaTextosPass.add(lbPass);

        cajaTextosPass.add(Box.createVerticalStrut(5));

        txtPass = new JPasswordField();
        txtPass.setHorizontalAlignment(JPasswordField.CENTER);
        txtPass.setPreferredSize(new Dimension(10,25));
        cajaTextosPass.add(txtPass);

        btnOlvidaste = new JButton("¿Olvidaste tu contraseña?");
        btnOlvidaste.setFont(fontBtn);
        cajaBotonesPass.add(btnOlvidaste);

        cajaBotonesPass.add(Box.createHorizontalStrut(10));

        btnSiguienteP = new JButton("Siguiente");
        btnSiguienteP.setFont(fontBtn);
        cajaBotonesPass.add(btnSiguienteP);

        cajaPrincipal.add(Box.createVerticalStrut(50)); // crea espacio entre el logo y el marco
        cajaPrincipal.add(cajaLogo); // agrega la cajaLogo a la caja principal

        cajaPrincipal.add(cajaTituloAcceder); //  agrega la caja tituloAcceder a la caja principal
        cajaPrincipal.add(cajaTituloBienvenida); // agrega caja tituloBienvenida a la caja principal
        cajaPrincipal.add(cajaUserDB);
        cajaPrincipal.add(Box.createVerticalStrut(80)); // crear espacio entre la cajaLogo y la cajaAcceder


        cajaDatos.add(cajaTextosUser);
        cajaDatos.add(cajaTextosPass);
        cajaDatos.add(Box.createVerticalStrut(15));
        cajaDatos.add(cajaBotonesUser);
        cajaDatos.add(cajaBotonesPass);

        cajaPrincipal.add(cajaDatos);
        cajaPrincipal.add(Box.createVerticalStrut(20));
        cajaPrincipal.add(cajaAlert);

        cajaPrincipal.add(Box.createVerticalStrut(20));
        cajaPrincipal.add(cajaProgressBar);

        panelFlow.add(cajaPrincipal);
        return panelFlow;
    }

    public JProgressBar progressBar;
    public JButton btnCrear, btnSiguienteU, btnOlvidaste, btnSiguienteP;
    public JPasswordField txtPass;
    public JTextField txtUser;
    public JLabel lbUserDB, lbAlert;
    JLabel lbLogo, lbAcceder, lbUser, lbBievenida, lbPass;
    private Box cajaPrincipal, cajaLogo, cajaDatos, cajaAlert, cajaProgressBar;
    public Box cajaTextosUser, cajaTextosPass, cajaBotonesUser,
            cajaBotonesPass, cajaTituloAcceder, cajaTituloBienvenida, cajaUserDB;
    public JPanel panelFlow;
}