package dao;

import connectionn.Connectionn;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserAccount_DAO {

    public UserAccount_DAO() {
        conn = new Connectionn();
    }

    /*
     * conaulta el usuario con un email
     * @param sql = establece la consulta sql
     * @param um de tipo clase User_Model para obtener el email y establecerlo como parametro
     *      al procedimiento almacenado
     */
    public boolean CheckAccount(String sql, String valueParameter) {
        try {
            cs = conn.Connect().prepareCall(sql);
            cs.setString(1, valueParameter);
            rs = cs.executeQuery();
            while (rs.next()){
                for (int i = 0; i < rs.getMetaData().getColumnCount(); i++) {
                    rsData.add(i, rs.getString(i+1));
                }
            }
            rs.close();
            return true;
        }catch (SQLException e) {
            e.printStackTrace();
            return false;
        }finally {
            conn.Disconnect();
        }
    }

    public ArrayList getRsData() {
        return rsData;
    }

    public String getSqlCheckAccout() {
        return sqlCheckAccout;
    }

    private ArrayList rsData = new ArrayList();

    private final String sqlCheckAccout = "{CALL SPR_CHECK_ACCOUNT(?)}";
    private final String sqlLocality = "{CALL SPR_LOCALITY(?)}";
    private final String sqlMunicipality = "{CALL SPR_MUNICIPALITY(?)}";
    private final String sqlStates = "{CALL SPR_STATE()}";

    private ResultSet rs;
    private CallableStatement cs;
    Connectionn conn;
}
