package connectionn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connectionn {
    public Connectionn() {
    }

    public Connection Connect() {
        try {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/db_sgdata",
                    "sgdata","d@ta/System*");
            System.out.println("-- Conectado a las base de datos--");
        }catch (SQLException e) {
            System.out.println("-- Falló la conexión a las base de datos--");
            e.printStackTrace();
        }finally {
            System.out.println("-- Metodo conectar finalizado --\n");
        }
        return conn;
    }
    
    public void Disconnect() {
        try {
            conn.close();
            System.out.println("-- Desconectado a las base de datos--");
        }catch (SQLException e) {
            System.out.println("-- Falló la desconexion a las base de datos--");
            e.printStackTrace();
        }finally {
            System.out.println("-- Metodo desconectar finalizado --\n");
        }
    }

    Connection conn;
}